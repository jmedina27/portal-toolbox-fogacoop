# Stage 0, based on Node.js, to build and compile Angular
# 'node' is the version name
#workdir,  new folder name for the app
FROM node:lastest as node
WORKDIR /app
COPY ./ /app/
RUN npm install
ARG
RUN npm run build -- --prod


#Stage 1, based on Nginx, to hace only the complied app, ready for production with Nginx

FROM nginx:alpine
COPY --from=node /app/dist/poc-sis-web /usr/share/nginx/html

#conf for routing angular
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf
