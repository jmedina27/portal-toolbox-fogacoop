export const states = {
    create: 'Created', 
    ready: 'Ready', 
    reserved: 'Reserved', 
    inProgress: 'InProgress', 
    suspended: 'Suspended', 
    completed: 'Completed', 
    failed: 'Failed', 
    error: 'Error', 
    exited: 'Exited', 
    obsolete: 'Obsolete'    
}

export const statesTaskActions = {
    claimed: 'claimed',
    released: 'released',
    delegated: 'delegated',
    completed: 'completed',
    activated: 'activated',
    forwarded: 'forwarded',
    nominated: 'nominated',
    exited: 'exited',
    started: 'started',
    stopped: 'stopped',
    suspended: 'suspended',
    resumed: 'resumed',
    failed: 'failed',
    skipped: 'skipped'
}

export const successResponse = {
    type: 'SUCCESS',
    statusCode: '200'
}

export const createdResponse = {
    type: 'SUCCESS',
    statusCode: '201'
}

export const userAuthenticationSuccessful = {
    type: 'SUCCESS',
    msg: 'Authentication successful',
    statusCode: '200'
}

export const failUserNotFound = {
    type: 'FAIL',
    msg: 'User not found',
    statusCode: '404'
}

export const failUserNotAuthorized = {
    type: 'FAIL',
    msg: 'User Not Authorized',
    statusCode: '204'
}

export const failureType = {
    error: 'ERROR',
    fail: 'FAIL'
}

export const failDemandNotFound = {
    type: 'ERROR',
    msg: 'Demand not found',
    statusCode: '404'
}

export const statesApiUser = {
    ok: 'OK'
}