// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
const hostApiBpm = 'http://192.168.1.52:8090/api';

const userData = 'http://192.168.1.52:8093/fogacoop/getData';
const userRoles = 'http://192.168.1.52:8093/fogacoop/roles';
const userAdm = 'http://192.168.1.52:8093/fogacoop';

//Ruta de servidor Pantallas IFRAME 
const iframesPath = 'http://192.168.1.52:8080/fogaweb/#/'
//const iframesPath = 'http://localhost:4201/#/'

const tokenApplication = '95d6aa45-b29e-489a-a8ea-ae1ee20e8de1';
const userOrganization = 'SOAINT';
const showDisabled = "true";

export const environment = {
    production: true,
    // Api BPM
    contenedor_endPoint: `${hostApiBpm}/container`,
    proceso_endPoint: `${hostApiBpm}/process`,
    tarea_endPoint: `${hostApiBpm}/task`,
    regla_endPoint: `${hostApiBpm}/rule`,
    // ApiUser
    user_endPoint: `${userAdm}/login`,
    userLogout_endPoint: `${userAdm}/logout`,
    token_Application: `${tokenApplication}`,
    userRecovery: `${userAdm}/recovery-pass`,
    changedPassword: `${userAdm}/changed`,
    // ApiUser/userData
    userListOrg: `${userData}/listUserOrg`,
    organization: `${userOrganization}`,
    user_update: `${userAdm}/update`,
    listaRoles: `${userRoles}/listaRoles`,
    showDisabled: `${showDisabled}`,
    //Iframe Path
    iframesPath: `${iframesPath}`



};
