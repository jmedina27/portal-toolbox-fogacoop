import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {UserDTO} from '../../../modelo/dto/userDTO';
import {NameValuePair} from 'src/app/modelo/so/nombreValor';
import {LoginService} from "../../../services/login.service";

import {RolesDTO} from "../../../modelo/dto/rolesDTO";
import {BodyDTO} from "../../../modelo/dto/common/domain/generic/user/bodyDTO";
import {UpdateDTO} from "../../../modelo/dto/updateDTO";
import {headerDTO} from "../../../modelo/dto/common/domain/generic/user/headerDTO";
import {UpdateBody} from "../../../modelo/dto/updateBody";
import {environment} from "../../../../environments/environment";
import {successResponse} from "../../../../environments/environment.variables";
import {UsuarioService} from "../../../services/usuario.service";
import {Router} from "@angular/router";
import {ROUTES_PATH} from "../../../app.routes.names";

@Component({
    selector: 'app-detalleAdministracionUsuarios',
    templateUrl: './detalleAdministracionUsuarios.component.html'
})
export class DetalleAdministracionUsuariosComponent implements OnInit {

    @Input() usuarioHijo: UserDTO;
    @Input() usuarioHijo1: BodyDTO;
    @Input() listaTipoIdent: NameValuePair[];
    @Input() listaRol: NameValuePair[];
    @Output() guardarUsuario = new EventEmitter<UserDTO>();
    display: boolean;
    rolSelect = new NameValuePair();
    tipoIdentSelect = new NameValuePair();
    newUser: boolean;
    currentUser: UserDTO;
    bandera:boolean=false;

    rolrol: RolesDTO;
    ides: string[] = [];
    listaRol12: RolesDTO[];
    listaRol1: BodyDTO;

    //

    constructor(private loginService: LoginService,
                private usuarioService: UsuarioService,
                private router: Router) {
        this.loginService.currentUser.subscribe(x =>
            this.currentUser = x);


        this.display = true;


        this.newUser = true;
    }


    ngOnInit() {

        if (!!this.usuarioHijo) {

            this.listaRol1 = JSON.parse(JSON.stringify(this.usuarioHijo));


            for (var rol in   this.listaRol1.roles) {

                this.rolrol = JSON.parse(JSON.stringify(this.listaRol1.roles[rol]));

                /*  this.listaRol.forEach(element => {
                      if (element.value != this.rolrol.description)
                      return;
                  })
     */
                this.ides.push(this.rolrol.description);
                this.bandera=true;
            }

            this.ides;


            /*this.listaRol.forEach(element => {
             if (element.value == this.usuarioHijo.body.id)
                this.rolSelect = element;
            });
      */


            /*this.listaTipoIdent.forEach(element => {
              if (element.value == this.usuarioHijo.identType)
                this.tipoIdentSelect = element;
            }); comentado erika*/
            if (!!this.usuarioHijo.body.username) {
                this.newUser = false;
            }
        }



    }

    guardarUsuarioInfo() {
        /*this.display = false;
       // this.usuarioHijo.identType = this.tipoIdentSelect.value;
       // this.usuarioHijo.state = states.create; // comentado erika
        if (!!this.newUser) {
          // this.usuarioHijo.role = this.rolSelect.value; // comentado erika
        } else {
          // this.usuarioHijo.newRole = this.rolSelect.value; // comentado erika
          this.usuarioHijo.body.password = null;
        }

        this.guardarUsuario.emit(this.usuarioHijo);
      }*/

        if (this.usuarioHijo1) {

            //   const userCO = { user: this.currentUser }
            const userCOE = new UpdateDTO();
            userCOE.header = new headerDTO;
            userCOE.body = new UpdateBody();
            let rolesUsers;
            const idUserRoles: string[] = [];
            userCOE.header.apiToken = environment.token_Application;
            userCOE.body.id = this.usuarioHijo1.id
            userCOE.body.username = this.usuarioHijo1.userName;
            userCOE.body.email = this.usuarioHijo1.email;
            userCOE.body.name = this.usuarioHijo1.name;
            userCOE.body.phoneNumber = this.usuarioHijo1.phoneNumber;
            userCOE.body.ldapGuid = null;
            userCOE.body.organization = this.usuarioHijo1.organization;

            for (var rol in  this.usuarioHijo1.roles) {
                rolesUsers = JSON.parse(JSON.stringify( this.usuarioHijo1.roles[rol]));
                idUserRoles.push(rolesUsers.id)
            }
            idUserRoles.push(this.rolSelect.value);
            userCOE.body.roles = idUserRoles;
            this.usuarioService.editarUsuario(userCOE)
                .subscribe(data => {
                        if (!!data && !!data.body)
                            data.body.status == successResponse.statusCode
                                ? this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.dashboard.url])
                                : null;

                    }, error => console.log(error)
                );

        }  /* else {

        }*/


        }


    cerrar() {
        this.usuarioHijo = null;
        this.guardarUsuario.emit(this.usuarioHijo);
    }


    eliminarRoles() {
        /*let rolesUsers;
        const idUserRoles: string[] = [];
        const userCOE = new UpdateDTO();
        userCOE.body = new UpdateBody();
        for (var rol in  this.usuarioHijo1.roles) {
            rolesUsers = JSON.parse(JSON.stringify( this.usuarioHijo1.roles[rol]));
            rolesUsers.id = null
            idUserRoles.push(rolesUsers.id)
        }
        idUserRoles.push(this.rolSelect.value);
        userCOE.body.roles = idUserRoles;
        this.usuarioService.editarUsuario(userCOE)
            .subscribe(data => {
                    if (!!data && !!data.body)
                        data.body.status == successResponse.statusCode
                            ? alert("Roles Eliminados exitosamente")
                            : null;
                }, error => console.log(error)
            );
        this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.adminUser]);

    }



*/
        if (this.usuarioHijo1) {

            //   const userCO = { user: this.currentUser }
            const userCOE = new UpdateDTO();
            userCOE.header = new headerDTO;
            userCOE.body = new UpdateBody();
            let rolesUsers;
            const idUserRoles: string[] = [];
            userCOE.header.apiToken = environment.token_Application;
            userCOE.body.id = this.usuarioHijo1.id
            userCOE.body.username = this.usuarioHijo1.userName;
            userCOE.body.email = this.usuarioHijo1.email;
            userCOE.body.name = this.usuarioHijo1.name;
            userCOE.body.phoneNumber = this.usuarioHijo1.phoneNumber;
            userCOE.body.ldapGuid = null;
            userCOE.body.organization = this.usuarioHijo1.organization;

            for (var rol in  this.usuarioHijo1.roles) {
                rolesUsers = JSON.parse(JSON.stringify( this.usuarioHijo1.roles[rol]));
                rolesUsers.id=null

                idUserRoles.push(rolesUsers.id)
            }
            idUserRoles.push(this.rolSelect.value=null);
            userCOE.body.roles = idUserRoles;
            this.usuarioService.editarUsuario(userCOE)
                .subscribe(data => {
                        if (!!data && !!data.body)
                            data.body.status == successResponse.statusCode
                                ? this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.dashboard.url])

                                : null;


                    }, error => console.log(error)
                );

        } /*  else {

        }*/
    }

}
