import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { ROUTES_PATH } from "src/app/app.routes.names";
import { LoginService } from "src/app/services/login.service";
import { UserDTO } from "src/app/modelo/dto/userDTO";
import { UtilitiesFnService } from "src/app/infraestructura/utils/utilitiesFn";
import { NameValuePair } from "src/app/modelo/so/nombreValor";
import { UsuarioService } from "src/app/services/usuario.service";
import { successResponse } from "src/environments/environment.variables";
import {LogOutDTO} from "../../../modelo/dto/LogOutDTO";
import {environment} from "../../../../environments/environment";
import {headerDTO} from "../../../modelo/dto/common/domain/generic/user/headerDTO";
import {BodyNoPassDTO} from "../../../modelo/dto/common/domain/generic/user/bodyNoPassDTO";
import {BodyDTO} from "../../../modelo/dto/common/domain/generic/user/bodyDTO";
import {RolesDTO} from "../../../modelo/dto/rolesDTO";
import {UpdateDTO} from "../../../modelo/dto/updateDTO";
import {UpdateBody} from "../../../modelo/dto/updateBody";

@Component({
    selector: 'app-perfilUsuario',
    templateUrl: './perfilUsuario.component.html'
})
export class PerfilUsuarioComponent implements OnInit {

    formUser: FormGroup;
    display: boolean = true;
    editarVsb: boolean = false;
    currentUser: UserDTO;
    gruposUsuario: NameValuePair[];
    tipoIdentList: NameValuePair[];
    identTypeSelect: NameValuePair;
    roleSelect: NameValuePair;

    constructor(
        private _formBuilder: FormBuilder,
        private router: Router,
        private loginService: LoginService,
        private fn: UtilitiesFnService,
        private usuarioService: UsuarioService
    ) {
        this.loginService.currentUser.subscribe(x => this.currentUser = x);

        this.formUser = this._formBuilder.group({
            identType: '',
            identNumber: '',
            name: '',
            lastName: '',
            fullName: '',
            email: '',
            role: '',
            newRole: '',
            phoneNumber: ''
        });

        this.tipoIdentList = [
            {name: 'Cédula Ciudadanía', value: '1'},
            {name: 'Tarjeta Identidad', value: '2'},
            {name: 'Tarjeta Pasaporte', value: '3'},
            {name: 'Registro Civil', value: '4'},
            {name: 'Cédula de Extranjería', value: '5'}
        ];
    }

    ngOnInit(): void {
        this.formUser.disable();

        this.onChargeEntity(this.currentUser);

        //this.cargarGrupos();

        this.onChargeForm();
    }

    onChargeEntity (user: UserDTO) {

      /*  if (!!user.body.) {
            this.formUser.controls['identType'].setValue(user.body.id);
            this.identTypeSelect = this.fn.filterItemByListNvp(this.tipoIdentList, user.body.id);
        }
       if (!!user.body.name)
            this.formUser.controls['identNumber'].setValue(user.identNumber);  comentado erika
       /* if (!!user.name)
            this.formUser.controls['name'].setValue(user.name);
        if (!!user.lastName)
            this.formUser.controls['lastName'].setValue(user.lastName);
        if (!!user.fullName)
            this.formUser.controls['fullName'].setValue(user.fullName);
        if (!!user.email)
            this.formUser.controls['email'].setValue(user.email); Comentado erika*/
       /* if (!!user.role)
            this.formUser.controls['newRole'].setValue(user.role); Comentado erika */
    }

    onChargeForm () {
        this.formUser.valueChanges
                     .subscribe( val => {
                        this.fn.formToDto(val, this.currentUser);
                     }, error => console.log(error)
                     );
    }

   /* cargarGrupos () {
        this.gruposUsuario = new Array<NameValuePair>();
        this.usuarioService.consultarGrupos(this.currentUser)
                           .subscribe(data => {
                            data.groups.forEach(element => {
                                var nvpGroup = new NameValuePair();
                                    nvpGroup.name = element.name;
                                    nvpGroup.value = element.name;
                                this.gruposUsuario.push(nvpGroup);
                            });
                            if (!!this.currentUser.role)
                                this.roleSelect = this.fn.filterItemByName(this.gruposUsuario, this.currentUser.role); // comentado erika
                           }, error => console.log(error))
                           ;
    }*/

    selectForm = function (event, controlName) {
        if (!!event.value) 
          this.formUser.controls[controlName].setValue(event.value.value);
    }

    aceptarPerfil () {
        this.display = false;
        if (!!this.editarVsb) {
         //   const userCO = { user: this.currentUser }
            const userCOE = new UpdateDTO();
            userCOE.header = new headerDTO;
            userCOE.body = new UpdateBody();
            let rolesUsers= new RolesDTO;
            const idUserRoles:string[]=[];
            userCOE.header.apiToken = environment.token_Application;
            userCOE.body.id = this.currentUser.body.id;
            userCOE.body.username = this.currentUser.body.username;
            userCOE.body.email = this.currentUser.body.email;
            userCOE.body.name = this.currentUser.body.name;
            userCOE.body.phoneNumber = this.currentUser.body.phoneNumber;
            userCOE.body.ldapGuid = null;
            userCOE.body.organization = this.currentUser.body.organization;
         //   userCOE.body.roles = this.currentUser.body.roles;
            for (var rol in  this.currentUser.body.roles) {
                rolesUsers = JSON.parse(JSON.stringify(this.currentUser.body.roles[rol]));
                idUserRoles.push(rolesUsers.id)
            }
            userCOE.body.roles = idUserRoles;
            this.usuarioService.editarUsuario(userCOE)
                               .subscribe(data => {
                                   if (!!data && !!data.body)
                                    data.body.status == successResponse.statusCode
                                                             ? this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.dashboard.url])
                                                             : null;                                  
                                }, error => console.log(error)
                                );
        } else {
            this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.dashboard.url]);
        }
    }

    editarPerfil () {
        this.formUser.enable();
        this.editarVsb = true;
    }

    logout() {
        this.loginService.logout();
        this.router.navigate(['/login']);
    }
   load() {
        if (typeof(Storage) != "undefined") {


        }
    }
}
