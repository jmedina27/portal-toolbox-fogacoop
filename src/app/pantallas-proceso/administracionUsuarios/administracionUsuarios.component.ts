import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ConfirmationService} from 'primeng/primeng';
import {UtilitiesFnService} from 'src/app/infraestructura/utils/utilitiesFn';
import {NameValuePair} from 'src/app/modelo/so/nombreValor';
import {UsuarioService} from 'src/app/services/usuario.service';
import {UserDTO} from '../../modelo/dto/userDTO';
import {LoginService} from 'src/app/services/login.service';
import {RolesDTO} from "../../modelo/dto/rolesDTO";
import {forEach} from "@angular/router/src/utils/collection";

@Component({
    selector: 'app-administracionUsuarios',
    templateUrl: './administracionUsuarios.component.html'
})
export class AdministracionUsuariosComponent implements OnInit {

    grupoUsuarios: UserDTO[];
    usuarioPadre: UserDTO;
    formUser: FormGroup;
    listaTipoIdent: NameValuePair[];
    listaRol: NameValuePair[];

    index: number = -1;
    currentUser: UserDTO;
    userAdmin: boolean;
    roles: RolesDTO[];

    constructor(
        private _formBuilder: FormBuilder,
        private fn: UtilitiesFnService,
        private confirmationService: ConfirmationService,
        private loginService: LoginService,
        private usuarioService: UsuarioService
    ) {
        this.loginService.currentUser.subscribe(x => this.currentUser = x);

        this.formUser = this._formBuilder.group({
            usuario: '',
            nombre: '',
            rol: ''
        });
    }

    ngOnInit() {

        this.currentUser;
        //this.role == "Administrador" ? this.userAdmin = true : this.userAdmin = false; //comentado erika

        this.cargarGrupos();

        this.listaTipoIdent = [
            {name: 'Cédula Ciudadanía', value: '1'},
            {name: 'Tarjeta Identidad', value: '2'},
            {name: 'Tarjeta Pasaporte', value: '3'},
            {name: 'Registro Civil', value: '4'},
            {name: 'Cédula de Extranjería', value: '5'}
        ];
    }


    cargarGrupos() {
        this.listaRol = new Array<NameValuePair>();
        this.usuarioService.consultarGrupos(this.currentUser)
            .subscribe(data => {
                data.body.body.forEach(element => {
                    var nvpGroup = new NameValuePair();
                    nvpGroup.name = element.description;
                    nvpGroup.value = element.id;
                    this.listaRol.push(nvpGroup);

                });
            }, error => console.log(error))
        ;
    }

    buscarGrupo() {
        let loginName = this.formUser.get("usuario").value;
        let userName = this.formUser.get("nombre").value;
        // let rolName = this.formUser.get("rol").value;

        this.usuarioService.consultarUsuarios(this.currentUser)
            .subscribe(data => {
                if (!!data && !!data.body) {
                    this.grupoUsuarios = new Array<UserDTO>();
                    if (!!loginName || !!userName) {
                        data.body.body.forEach(user => {
                            if (user.name == loginName || user.userName == userName)
                                this.grupoUsuarios.push(user);
                        });
                    } else {
                        this.grupoUsuarios = data.body.body;
                    }
                }
            });

    }

    //////
    /* obtenerRoles() {
       let roles : [];
       this.usuarioService.obtenerRoles( this.currentUser)
           .subscribe(data =>{ roles = data.body.description })
     }*/

///////


    nuevoUsuario() {
        this.usuarioPadre = new UserDTO();
    }

    editarUsuario(user, index) {
        this.usuarioPadre = user;
        this.index = index;
    }

    guardarUsuario(newUser: UserDTO) {
        if (newUser != null) {
            const userCO = {user: newUser}
            if (this.index == -1) {
                this.usuarioService.crearUsuario(userCO).subscribe(data => {
                    debugger
                });
            } else {
                this.usuarioService.editarUsuario(userCO).subscribe(data => {
                    debugger
                });
                // newUser.role = newUser.newRole; //comentado erika
            }
            this.fn.updateItemByEntity(this.grupoUsuarios, newUser, this.index);
        }
        this.usuarioPadre = null;
        this.index = -1;
    }

    eliminarUsuario(usuario: UserDTO) {
        this.confirmationService.confirm({
            message: '¿Realmente desea eliminar el registro?',
            accept: () => {
                const userCO = {user: usuario}
                this.usuarioService.eliminarUsuario(userCO).subscribe(data => {
                    debugger
                });
                this.grupoUsuarios = this.fn.deleteItemByEntity(this.grupoUsuarios, usuario);
            },
        });
    }

    valorLista(item: string, lista: any[]) {
        const itemNvp = this.fn.filterItemByListNvp(lista, item);
        return !!itemNvp ? itemNvp.name : '';
    }
}
