import {environment} from "../../../environments/environment";
import {DomSanitizer} from "@angular/platform-browser";
import {ActivatedRoute} from "@angular/router";
import {Component} from "@angular/core";
@Component({
    selector: 'app-enrutador-pantallas',
    templateUrl: './enrutador-pantallas.component.html',
    styleUrls: ['./enrutador-pantallas.component.css']
})

export class EnrutadorPantallasComponent {

    constructor(public sanitizer: DomSanitizer, public  route: ActivatedRoute) {
    }

    iframePath() {
        let urlIframe = this.route.snapshot.params["redirect"];
        let url = environment.iframesPath + urlIframe;
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
}

