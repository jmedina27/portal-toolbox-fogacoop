import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cabeceraProceso',
  template: `
      <div class="ui-g ui-fluid">
        <div class="ui-g-6">
          <img src = "assets/layout/images/sdisLogo.png" style="float:left">
        </div>
        <div class="ui-g-6">
          <img src ="assets/layout/images/soaintLogo.png" style="float:right">
        </div>
      </div>
  `
})
export class CabeceraProcesoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
