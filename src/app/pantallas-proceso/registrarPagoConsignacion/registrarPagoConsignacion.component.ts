import {Component, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-registrar-pago-consignacion',
    templateUrl: '../enrutador-pantallas/enrutador-pantallas.component.html',
    styleUrls: ['../enrutador-pantallas/enrutador-pantallas.component.css']
})

export class RegistrarPagoConsignacionComponent implements OnInit {

    constructor(public sanitizer: DomSanitizer, public  route: ActivatedRoute) {
    }

    ngOnInit() {
    }
}
