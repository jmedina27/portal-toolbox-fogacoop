import {AfterViewInit, Component, OnInit} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {ROUTES_PATH} from "../../app.routes.names";
import {environment} from "../../../environments/environment";

@Component({
    selector: 'app-abono-devolucion',
    templateUrl: '../gestion-psd/gestion-psd.component.html',
    styleUrls: ['../enrutador-pantallas/enrutador-pantallas.component.css']
})
export class AbonoDevolucionComponent implements OnInit, AfterViewInit {

    url: SafeResourceUrl;

    constructor(private sanitizer: DomSanitizer,
                private router: Router) {}

    ngOnInit() {
    }

    frameURL() {
        let url = environment.iframesPath + "abonoDevolucionPago?param=" + btoa(localStorage.getItem('proccessCurrent'));
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }


    ngAfterViewInit() {
        var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
        var eventer = window[eventMethod];
        var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
        eventer(messageEvent, (e) => {
            if (e.data == "redirect") {
                this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.task.url]);
            }
        }, false);
    }


}
