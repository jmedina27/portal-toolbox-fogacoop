import { Component, OnInit, ChangeDetectorRef, ViewRef } from '@angular/core';
import { TareaDTO } from 'src/app/modelo/dto/tareaDTO';
import { ProcessRequestDTO } from 'src/app/modelo/dto/processRequestDTO';
import { TareaService } from 'src/app/services/tarea.service';
import { AlertService } from 'src/app/infraestructura/utils/alert.service';
import { LoginService } from 'src/app/services/login.service';
import { states, createdResponse } from 'src/environments/environment.variables';
import { UserDTO } from 'src/app/modelo/dto/userDTO';
import { Router } from '@angular/router';
import { ROUTES_PATH } from 'src/app/app.routes.names';
import { UserBPMDTO } from 'src/app/modelo/dto/UserBPMDTO';
import {BodyDTO} from "../../../modelo/dto/common/domain/generic/user/bodyDTO";

@Component({
  selector: 'app-reasignarTarea',
  templateUrl: './reasignarTarea.component.html'
})
export class ReasignarTareaComponent implements OnInit {

  tareas: TareaDTO[];
  request = new ProcessRequestDTO;

  constructor(
    private cd: ChangeDetectorRef,
    private tareaService: TareaService,
    private alertService: AlertService,
    private router: Router,
    private loginService: LoginService
  ) {
        //Tomando usuario del Login para el request JBPM
        this.request.ownerUser = new UserBPMDTO;
        this.loginService.currentUser.subscribe(data => { 
          this.request.ownerUser.user = data.body.username;
          this.request.ownerUser.password = data.body.password;
        });
  }

  ngOnInit() {
   if (!!this.request.ownerUser)
      this.cargarTareasPorGrupo()
    //this.request.assignment = new UserDTO();
    this.request.assignment = new BodyDTO();
  }

  cargarTareasPorGrupo () {
    this.request.groups = new Array<string>();
    this.tareaService.consultarTareasPorGrupos(this.request)
                     .subscribe(data => {
                        if (!!data && !!data.containers && !!data.containers[0].processes) {
                          this.tareas = new Array<TareaDTO>();
                          data.containers[0].processes[0].taskList.forEach(element => {
                            if (element.taskStatus == states.create || 
                                element.taskStatus == states.inProgress ||
                                element.taskStatus == states.reserved)
                              this.tareas.push(element);
                          });
                        }
                     });
  }

  detectChanges() {
    setTimeout(() => {
      if (!(this.cd as ViewRef).destroyed) {
        this.cd.detectChanges();
      }
    }, 250);
  }
  
  reasignarTarea (task: TareaDTO) {
    if (!!task) {
      this.request.assignment.user = task.taskActualOwner;
      this.request.containerId = task.containerId;
      this.request.taskId = task.taskId.toString();
    }
  }

  enviarAsignacion (valor: string) {
    if (!!valor) {
       this.request.assignment.targetUser = valor;
      this.tareaService.reasignarTarea(this.request)
                       .subscribe(data => {
                         if (!!data.response && data.response.statusCode == createdResponse.statusCode)
                          this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.task]);
                       });
    }
    this.request.assignment.targetUser = null;
    this.request.assignment.user = null;
  }

}
