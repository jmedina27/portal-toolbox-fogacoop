import { Component, OnInit } from '@angular/core';
import {ConfirmacionPagoDTO} from "../../modelo/dto/ConfirmacionPagoDTO";
import {PseService} from "../../services/pse.service";

@Component({
  selector: 'app-confirmar-pago',
  templateUrl: './confirmar-pago.component.html',
  styleUrls: ['./confirmar-pago.component.css']
})
export class ConfirmarPagoComponent implements OnInit {

    value: Date;
    cus: string;
    confirmacionPago: ConfirmacionPagoDTO = {
        entidad: '',
        NIT: '',
        fechaPago: null,
        estadoPago: '',
        numTransaccion: null,
        banco: null,
        valorPagado: null,
        descripcion: 'Azul'
    };

    constructor(private getTransactionService: PseService) {

    }


    ngOnInit() {
        this.consultarEstadoTransaction();

    }


    consultarEstadoTransaction(): any {
        this.getTransaction(0, 7);

    }

    getTransaction(cont: number, i: number) {
        if (cont < i) {
            this.getTransactionService.getTransaction().subscribe((data) => {
                this.confirmacionPago.banco = data.body.bankName;
                this.confirmacionPago.NIT = localStorage.getItem("nitCooperativa");
                this.confirmacionPago.entidad = localStorage.getItem("nombreCoop");
                switch (data.body.tranState) {
                    case 'OK':
                        this.confirmacionPago.estadoPago = "APROBADO";
                        break;
                    case 'PENDING':
                        this.confirmacionPago.estadoPago = "PENDIENTE";
                        break;
                    case 'NOT_AUTHORIZED':
                        this.confirmacionPago.estadoPago = "RECHAZADA";
                        break;
                    case 'FAILED':
                        this.confirmacionPago.estadoPago = "FALLIDA";
                        break;
                }
                this.confirmacionPago.fechaPago = data.body.bankProcessDate;


                this.confirmacionPago.numTransaccion = data.body.ticketId;
                this.confirmacionPago.valorPagado = data.body.transValue;
                this.cus = data.body.trazabilityCode;
                localStorage.setItem("estadoPago", this.confirmacionPago.estadoPago);
                localStorage.setItem("cus", this.cus);
            });
            setTimeout(() => {
                this.getTransaction(cont + 1, i)
            }, 180000);


        }


    }


    Finalizar() {

    }

}
