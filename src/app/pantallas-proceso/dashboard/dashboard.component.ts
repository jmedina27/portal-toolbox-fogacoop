import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProcessRequestDTO } from 'src/app/modelo/dto/processRequestDTO';
import { LoginService } from 'src/app/services/login.service';
import { TareaService } from 'src/app/services/tarea.service';
import { states } from 'src/environments/environment.variables';
import { UserDTO } from 'src/app/modelo/dto/userDTO';
import { UserBPMDTO } from 'src/app/modelo/dto/UserBPMDTO';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit, OnDestroy {

  tasksStadistics: any[];
  subscriptions: Subscription[] = [];
  totalTask = 0;
  completedTasks = 0;
  progressTasks = 0;
  readyTasks = 0;
  completedTasksPercents = 0;
  progressTasksPercents = 0;
  readyTasksPercents = 0;
  data: any;
  request = new ProcessRequestDTO;

  constructor(
    private loginService: LoginService,
    private tareaService: TareaService
  ) {
    //Tomando usuario del Login
    this.request.ownerUser = new UserBPMDTO;
    this.loginService.currentUser.subscribe(data => {
        //console.log(data);
      this.request.ownerUser.user = data.body.username;
      this.request.ownerUser.password = data.body.password;
    });
  }

  ngOnInit() {
    if (!!this.request.ownerUser){
      this.cargarDashboard();
    }

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach( s => s.unsubscribe());
  }

  cargarDashboard () {
    this.request.groups = new Array<string>();
    this.tareaService.consultarTareasPorGrupos(this.request)
                     .subscribe(data => {
                        if (!!data && !!data.containers && !!data.containers[0].processes) {
                          this.totalTask = data.containers[0].processes[0].taskList.length;
                          data.containers[0].processes[0].taskList.forEach(element => {
                            if (element.taskStatus == states.ready ||
                                element.taskStatus == states.create ||
                                element.taskStatus == states.reserved)
                              this.readyTasks++;
                            if (element.taskStatus == states.inProgress)
                              this.progressTasks++;
                            if (element.taskStatus == states.completed)
                              this.completedTasks++;
                          });
                          this.readyTasksPercents = this.readyTasks * 100 / this.totalTask;
                          this.readyTasksPercents = Number(this.readyTasksPercents.toFixed(2));

                          this.progressTasksPercents = this.progressTasks * 100 / this.totalTask;
                          this.progressTasksPercents = Number(this.progressTasksPercents.toFixed(2));

                          this.completedTasksPercents = this.completedTasks * 100 / this.totalTask;
                          this.completedTasksPercents = Number(this.completedTasksPercents.toFixed(2));

                          this.cargarGrafica();
                        }
                     });
  }

  cargarGrafica() {
      this.data = {
          labels: ['En proceso','Completadas', 'Pendiente por gestión'],
          datasets: [
              {
                  data: [this.progressTasks, this.completedTasks, this.readyTasks],
                  backgroundColor: [
                      "#4527A0",
                      "#2E7D32",
                      "#0277BD"
                  ],
                  hoverBackgroundColor: [
                      "#4527A0",
                      "#2E7D32",
                      "#0277BD"
                  ]
              }]
          };
  }
}
