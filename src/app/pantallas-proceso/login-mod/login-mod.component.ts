import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {UserDTO} from 'src/app/modelo/dto/userDTO';
import {AlertService} from '../../infraestructura/utils/alert.service';
import {LoginService} from '../../services/login.service';
import {MessageService} from "primeng/api";
import {BodyDTO} from "../../modelo/dto/common/domain/generic/user/bodyDTO";
import {headerDTO} from "../../modelo/dto/common/domain/generic/user/headerDTO";
import {environment} from "../../../environments/environment";


@Component({
    selector: 'app-login-mod',
    templateUrl: './login-mod.component.html'
})

export class LoginModComponent implements OnInit {
    recuperar = false;
    form: FormGroup;
    mensaje: string = "";


    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private loginService: LoginService,
        private alertService: AlertService,
        private messageService: MessageService
    ) {
        // redirect to home if already logged in
        if (this.loginService.currentUserValue)
            this.router.navigate(['/']);
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    submit() {
        if (this.form.invalid) {
            return;
        }
        const user = new UserDTO();
        const body = new BodyDTO();
        const header = new headerDTO();
        body.username = this.form.get('username').value;
        body.password = this.form.get('password').value;
        header.apiToken = environment.token_Application;
        user.header = header;
        user.body = body;
        this.loginService.login(user)
            .pipe(first())
            .subscribe(data => {
                if (!!data)
                    this.router.navigate(['/home']);
            });
    }

    recuperarPassword() {
        this.recuperar = true;
    }

    closePopup() {
        this.recuperar = false;
    }

    recuperarService() {
        if (this.form.invalid) {
            return;
        }
        let user = new UserDTO();
        let header = new headerDTO();
        let body = new BodyDTO();
        body.username = this.form.get('username').value;
        body.password = this.form.get('password').value;
        header.apiToken = environment.token_Application;

        user.header = header;
        user.body = body;

        this.loginService.recoverPassword(user).subscribe(data => {
            console.log(data);
            this.mensaje = data.message;

            if (data.status == 'OK') {
                this.closePopup();
                this.messageService.add({severity: 'success', summary: '¡Correcto!', detail: this.mensaje});
            }

            if (data.status !== 'OK') {
                this.messageService.add({
                    severity: 'error',
                    summary: '¡Atención!',
                    detail: "Error al cambiar la contraseña!"
                });
            }
        });
    }
}
