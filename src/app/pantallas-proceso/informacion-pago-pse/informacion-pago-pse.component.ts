import { Component, OnInit } from '@angular/core';

import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import {MessageService} from "primeng/api";
import {Message} from 'primeng/components/common/api';
import {environment} from "../../../environments/environment.prod";
import {RequestCreateTransactionDTO} from "../../modelo/dto/RequestCreateTransactionDTO";
import {PseService} from "../../services/pse.service";
@Component({
  selector: 'app-informacion-pago-pse',
  templateUrl: './informacion-pago-pse.component.html',
  styleUrls: ['./informacion-pago-pse.component.css']
})
export class InformacionPagoPSEComponent implements OnInit {
    formPagoPse: FormGroup;
    ticketId: string;
    urlPago: string;
    estadoPago: string;
    pagar: boolean = false;
    numeroTransaccion: string;
    cus: string;
    msgs: Message[] = [];
    requestCreatetransacion = new RequestCreateTransactionDTO();


    constructor(private messageService: MessageService,
                private getTransactionService: PseService,
                private formBuilder: FormBuilder,
                private createTransactionService: PseService) {

    }


    validaciones() {
        this.formPagoPse = this.formBuilder.group({
            nit: ['CC', Validators.required],
            nitCooperativa: ['', Validators.required],
            nombreCooperativa: ['', Validators.required],
            direccionCooperativa: ['', Validators.required],
            telefonoCooperativa: ['', Validators.required],
            valorPagar: ['', Validators.required],
            correoCooperativa: ['', Validators.required]

        });
    }

    consultarEstadoTransaction(): any {
        this.getTransaction(0, 7);

    }

    getTransaction(cont: number, i: number) {
        if (cont < i) {
            this.getTransactionService.getTransaction().subscribe((data) => {
                this.estadoPago = data.body.tranState;
                if (this.estadoPago == "CREATED") {
                    this.msgs = [];
                    this.msgs.push({
                        severity: 'warn',
                        summary: 'Warn Message',
                        detail: 'En este momento su factura ' + localStorage.getItem("ticket") + ' presenta un proceso de pago cuya transaccion se encuentra PENDIENTE'
                    });
                    this.pagar = true;
                } else if (this.estadoPago == "PENDING") {
                    this.msgs = [];
                    this.msgs.push({
                        severity: 'warn',
                        summary: 'Warn Message',
                        detail: '\n En este momento su factura <' + localStorage.getItem("ticket") + '> presenta un proceso de pago cuya transaccion se encuentra PENDIENTE \n'
                            + 'Si desea mayor información sobre el estado actual de su operación puede comunicarse a nuestras lineas de atención 666-666 \n o enviar un correo electronico a correo@gmail.com  y preguntar por el estado de la transaccion <'
                            + localStorage.getItem("cus") + '>'
                    });
                    this.pagar = true;

                } else if (this.estadoPago == "OK" || this.estadoPago == ""){
                    this.msgs = [];
                    this.pagar = false;
                    cont = 7;
                }
                else if (this.estadoPago == "NOT_AUTORIZATHED") {
                    this.msgs=[];
                    this.msgs.push({
                        severity: 'warn',
                        summary: 'Warn Message',
                        detail: '\n En este momento su factura <' + localStorage.getItem("ticket") + '> presenta un proceso de pago cuya transaccion fue RECHAZADA.\n'
                            + 'Si desea mayor información sobre el estado actual de su operación puede comunicarse a nuestras lineas de atención 666-666 \n o enviar un correo electronico a correo@gmail.com  y preguntar por el estado de la transaccion <'
                            + localStorage.getItem("cus") + '>'
                    });
                    this.pagar=false;
                    cont = 7;
                }
                else if (this.estadoPago == "FAILED") {
                    this.msgs=[];
                    this.msgs.push({
                        severity: 'warn',
                        summary: 'Warn Message',
                        detail: '\n En este momento su factura <' + localStorage.getItem("ticket") + '> presenta un proceso de pago cuya transaccion fue FALLIDA.\n'
                            + 'Si desea mayor información sobre el estado actual de su operación puede comunicarse a nuestras lineas de atención 666-666 \n o enviar un correo electronico a correo@gmail.com  y preguntar por el estado de la transaccion <'
                            + localStorage.getItem("cus") + '>'
                    });
                    this.pagar=false;
                    cont = 7;
                }
                else
                    this.pagar = false;
            });
            setTimeout(() => {
                this.getTransaction(cont + 1, i)
            }, 180000);


        }


    }

    ngOnInit() {

        this.validaciones();
        this.consultarEstadoTransaction()

    }

    crearRequest() {

        // requestCreatetransacion.ReferenceArray = new ReferenceArrayDTO();
        this.requestCreatetransacion.entityCode = environment.entityCode;
        this.requestCreatetransacion.srvCode = environment.srvCode;
        this.requestCreatetransacion.transValue = this.formPagoPse.get("valorPagar").value;
        const referenceArray: string[] = [];
        for (let i = 0; i <= 0; i++) {
            referenceArray[0] = this.formPagoPse.get("nitCooperativa").value;
            referenceArray[1] = this.formPagoPse.get("nit").value;
            referenceArray[2] = this.formPagoPse.get("direccionCooperativa").value;
            referenceArray[3] = this.formPagoPse.get("telefonoCooperativa").value;
            referenceArray[4] = this.formPagoPse.get("correoCooperativa").value;
        }
        this.requestCreatetransacion.urlRedirect = environment.urlRedirect;
        console.log(referenceArray);
        this.requestCreatetransacion.referenceArray = referenceArray;
    }

    guardarLocalStorage() {

        localStorage.setItem("nombreCoop", this.formPagoPse.get("nombreCooperativa").value);
        localStorage.setItem("nitCooperativa", this.formPagoPse.get("nitCooperativa").value);
    }


    createTransaction() {
        this.createTransactionService.createTransaction(this.requestCreatetransacion).subscribe((data) => {
                console.log(data.body);
                this.ticketId = data.body.ticketId;
                this.urlPago = data.body.ecollectUrl;
                // @ts-ignore
                window.location = this.urlPago;
                localStorage.setItem("ticket", this.ticketId);
            }
        );

    }


    Pagar() {


     //   localStorage.clear();
        this.crearRequest();
        this.createTransaction();
        this.guardarLocalStorage();

    }

}
