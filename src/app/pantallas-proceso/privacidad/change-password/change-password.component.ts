import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {changedPasswordDTO} from "../../../modelo/dto/changedPasswordDTO";
import {ChangedPassService} from "../../../services/changed-pass.service";
import {MessageService} from "primeng/api";
import {bodyChangedPasswordDTO} from "../../../modelo/dto/common/domain/generic/user/bodyChangePasswordDTO";
import {headerDTO} from "../../../modelo/dto/common/domain/generic/user/headerDTO";
import {environment} from "../../../../environments/environment";
import {UserDTO} from "../../../modelo/dto/userDTO";
import {UsuarioService} from "../../../services/usuario.service";
import {LoginService} from "../../../services/login.service";
import {BodyDTO} from "../../../modelo/dto/common/domain/generic/user/bodyDTO";

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html'
})
export class ChangePasswordComponent implements OnInit {
    cambiar = false;
    form: FormGroup;
    mensaje = "";
    currentUser: UserDTO;
    body: BodyDTO;

    constructor(private changePassService: ChangedPassService,
                private messageService: MessageService,
                private formBuilder: FormBuilder,
                private usuarioService: UsuarioService,
                private loginService: LoginService,
    ) {
        this.loginService.currentUser.subscribe(x => this.currentUser = x);
    }

    ngOnInit(){this.body = this.currentUser.body;
    this.form = this.formBuilder.group({
        username: ['', Validators.required],
        oldPassword: ['', Validators.required],
        newPassword: ['', Validators.required],


    });
    }

    cambiarPassword() {
        this.cambiar = true;

    }

    closePopup() {
        this.cambiar = false;
    }

    cambiarService() {
        if (this.form.invalid) {
            return;
        }
        let change = new changedPasswordDTO();
        let header = new headerDTO();
        let body = new bodyChangedPasswordDTO();
        body.userName = this.form.get('username').value;
        body.oldPassword = this.form.get('oldPassword').value;
        body.newPassword = this.form.get('newPassword').value;
        header.apiToken =  environment.token_Application;

        change.header = header;
        change.body = body;

        this.changePassService.changedPassword(change).subscribe(data => {
            console.log(data);

            if (data.status == 'OK') {
                this.closePopup();
                this.messageService.add({severity: 'success', summary: '¡Correcto!', detail: this.mensaje});
            }

            if (data.status !== 'OK') {
                this.messageService.add({
                    severity: 'error',
                    summary: '¡Atención!',
                    detail: "Error al cambiar la contraseña!"
                });
            }
        });
    }

}
