import {ADMINISTRADOR, COOPERATIVA, USUARIO} from "./app.roles";

export const ROUTES_PATH = {
    home: 'home',
    login: 'login',
    dashboard: {url: 'dashboard' ,rol:  [ADMINISTRADOR, USUARIO]},
    adminUser: 'adminUser',
    privacidad: 'privacidad',
    userProfile: 'userProfile',
    processes: {url: 'processes', rol: [ADMINISTRADOR, USUARIO]},
    task: {url: 'tasks', rol: [ADMINISTRADOR, USUARIO]},
    reassignTask: {url: 'reassignTask', rol: [ADMINISTRADOR, USUARIO]},
    enrutadorPantalla: {url: 'enrutadorPantalla', rol:  [ADMINISTRADOR, COOPERATIVA, USUARIO]},
    registrarPagoConsignacion: {url: 'rPagoConsignacion', rol: [ADMINISTRADOR, COOPERATIVA, USUARIO]},
    informacionPagoPSE: {url: 'infoPago', rol: [ADMINISTRADOR, COOPERATIVA, USUARIO]},
    confirmarPago: {url: 'confirmacionPago', rol: [ADMINISTRADOR, COOPERATIVA, USUARIO]},
    consultaCooperativa: {url: 'consultaMovimientosCooperativa', rol:  [ADMINISTRADOR, COOPERATIVA, USUARIO]},
    consultaFogacoop: {url: 'consultaMovimientosCooperativaFogacoop', rol:  [ADMINISTRADOR, COOPERATIVA, USUARIO]},
    informacionPagoNoLiquidado: {url: 'infoPagoNoLiquidado', rol:  [ADMINISTRADOR, COOPERATIVA, USUARIO]},
    gestionPagosPsd: {url: 'GestionarpagosPSD', rol:  [ADMINISTRADOR, COOPERATIVA, USUARIO]},
    abonoDevolucion: {url: 'Confirmarabonodevolucion', rol:  [ADMINISTRADOR, COOPERATIVA, USUARIO]}
};
