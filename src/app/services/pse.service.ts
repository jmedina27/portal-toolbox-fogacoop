import { Injectable } from "@angular/core";
import { ApiManagerService } from "../infraestructura/api/api-manager";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import {ResponseCreateTransactionDTO} from "../modelo/dto/ResponseCreateTransactionDTO";
import {RequestCreateTransactionDTO} from "../modelo/dto/RequestCreateTransactionDTO";


@Injectable({
    providedIn: 'root'
})
export class PseService {
    ticketId: string;
    constructor(private api: ApiManagerService) {
    }

    createTransaction(transaction: RequestCreateTransactionDTO): Observable<ResponseCreateTransactionDTO> {
        const endpoint = environment.createTransaction;
        console.log(transaction);
        return this.api.post(endpoint, transaction);

    }

    getTransaction(): Observable<any> {
        const endpoint = environment.getTransaction;
        this.ticketId= localStorage.getItem("ticket");
        const params = `entityCode=${environment.entityCode}&ticketId=${this.ticketId}`
        console.log(params);
        return this.api.get(endpoint, params );


    }
}

