import { Injectable } from "@angular/core";
import { ApiManagerService } from "../infraestructura/api/api-manager";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { UserDTO } from "../modelo/dto/userDTO";
//import { rolesDTO } from "../modelo/dto/rolesDTO";

@Injectable()
export class UsuarioService {

    constructor(
        private _api: ApiManagerService
    ) {}

    consultarGrupos (user: UserDTO) : Observable<any> {
        const endpoint = environment.listaRoles ;
        user.header.apiToken = environment.token_Application;
        const showDisabled = environment.showDisabled;
        const params =`showDisabled=${showDisabled}&apiToken=${user.header.apiToken}`

        return this._api.get(endpoint,params);
    }

    consultarUsuarios (user: UserDTO ) : Observable<any> {

        const endpoint = environment.userListOrg ;
        user.header.apiToken = environment.token_Application;
        //
        const organization = environment.organization;
        //
        const params = `apiToken=${user.header.apiToken}&organization=${organization}`
        //const params =`apiToken=${user.header.apiToken}&organization=${user.body.organization}`

        return this._api.get(endpoint,params);
    }
    //
    /*obtenerRoles (roles: rolesDTO ): Observable<any> {
        const endpoint = environment.roles ;
        return this._api.get(endpoint);

    }*/

    crearUsuario (user: any) : Observable<any> {
        const endpoint = environment.user_endPoint + '/user';
        return this._api.put(endpoint, user);
    }

    editarUsuario (user: any) : Observable<any> {
        const endpoint = environment.user_update;
        return this._api.put(endpoint, JSON.stringify(user));
    }

    eliminarUsuario (user: any) : Observable<any> {
        const endpoint = environment.user_endPoint + '/user';
        return this._api.delete(endpoint, user);
    }

}
