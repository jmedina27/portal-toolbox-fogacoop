import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {LiquidacionPsdDTO} from "../../modelo/dto/LiquidacionPsdDTO";
@Injectable({
    providedIn: 'root',
})
export class LiquidacionPSDService {

    private dataLiquidacion: LiquidacionPsdDTO[] = [
        {
            vr_total_a_pagar: 34000,
            vr_base_psd: 2300,
            vr_depositos: 12000,
            posicion_neta_val: 123,
            saldo_a_favor_antes: 123455,
            saldo_a_cargo: 5000,
            interes_Mora: 23455,
            dias_Mora: 2,
            tasa_Mora: 2000000
        },
        {
            vr_total_a_pagar: 7000,
            vr_base_psd: 200,
            vr_depositos: 2000,
            posicion_neta_val: 1,
            saldo_a_favor_antes: 44444,
            saldo_a_cargo: 3000,
            interes_Mora: 1455,
            dias_Mora: 10,
            tasa_Mora: 8000
        }
    ];

    constructor(private http: HttpClient) { }

    getLiquidacionPSD() {
        return this.dataLiquidacion;
    }


}
