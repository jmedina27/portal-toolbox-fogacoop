import { Injectable } from "@angular/core";
import { Router, CanActivate } from "@angular/router";
import { LoginService } from "./login.service";

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(
        private authService: LoginService, 
        private router: Router
    ) { }

    canActivate() {
        if (!this.authService.currentUserValue) {
            console.log('No estás logueado');
            this.router.navigate(['/login']);
            return false; 
        }
        return true;
    }
}
