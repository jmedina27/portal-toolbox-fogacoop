import {Injectable} from '@angular/core';
import {ApiManagerService} from "../infraestructura/api/api-manager";
import {environment} from "../../environments/environment";
import {changedPasswordDTO} from "../modelo/dto/changedPasswordDTO";

@Injectable({
    providedIn: 'root'
})

export class ChangedPassService {

    constructor(private _api: ApiManagerService) {
    }


    changedPassword(change: changedPasswordDTO) {
        const endpoint = environment.changedPassword;
        return this._api.post(endpoint, change)
    }
}
