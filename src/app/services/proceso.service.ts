import { Injectable } from "@angular/core";
import { ApiManagerService } from "../infraestructura/api/api-manager";
import { ProcessRequestDTO } from "../modelo/dto/processRequestDTO";
import { ProcessResponseDTO } from "../modelo/dto/processResponseDTO";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";

@Injectable()
export class ProcesoService {

    constructor(
        private _api: ApiManagerService
    ) {}

    consultarProcesos (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.proceso_endPoint + '/processesByContainer';
        return this._api.post(endpoint, request);
    }

    iniciarProceso (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.proceso_endPoint + '/startProcessInstance';
        return this._api.post(endpoint, request);
    }
}