import {RouterModule, Routes} from '@angular/router';
import {ROUTES_PATH} from './app.routes.names';
import {AdminLayoutComponent} from './infraestructura/layout-components/adminLayoutComponent/adminLayout/adminLayout.component';
import {DashboardComponent} from './pantallas-proceso/dashboard/dashboard.component';
import {LoginModComponent} from './pantallas-proceso/login-mod/login-mod.component';
import {AdministracionUsuariosComponent} from './pantallas-proceso/administracionUsuarios/administracionUsuarios.component';
import {PerfilUsuarioComponent} from './pantallas-proceso/administracionUsuarios/perfilUsuario.component/perfilUsuario.component';
import {AdministracionProcesosComponent} from './pantallas-proceso/administracionProcesos/administracionProcesos.component';
import {AuthGuardService} from './services/authGuard.service ';
import {AdministracionTareasComponent} from './pantallas-proceso/administracionTareas/administracionTareas.component';
import {ReasignarTareaComponent} from './pantallas-proceso/administracionTareas/reasignarTarea/reasignarTarea.component';
import {ChangePasswordComponent} from "./pantallas-proceso/privacidad/change-password/change-password.component";
import {ManagerGuard} from "./infraestructura/shared/manager.guard";
import {GestionPSDComponent} from "./pantallas-proceso/gestion-psd/gestion-psd.component";
import {AbonoDevolucionComponent} from "./pantallas-proceso/abono-devolucion/abono-devolucion.component";
import {EnrutadorPantallasComponent} from './pantallas-proceso/enrutador-pantallas/enrutador-pantallas.component';

export const routes: Routes = [

    {path: ROUTES_PATH.login, component: LoginModComponent},
    {path: '', redirectTo: ROUTES_PATH.home, pathMatch: 'full'},
    {
        path: ROUTES_PATH.home,
        component: AdminLayoutComponent,
        canActivate: [AuthGuardService],
        children: [
            {path: ROUTES_PATH.dashboard.url, component: DashboardComponent, canActivate: [ManagerGuard]},
            {path: ROUTES_PATH.adminUser, component: AdministracionUsuariosComponent, canActivate: [ManagerGuard]},
            {path: ROUTES_PATH.userProfile, component: PerfilUsuarioComponent},
            {path: ROUTES_PATH.privacidad, component: ChangePasswordComponent},
            {path: ROUTES_PATH.processes.url, component: AdministracionProcesosComponent, canActivate: [ManagerGuard]},
            {path: ROUTES_PATH.task.url, component: AdministracionTareasComponent, canActivate: [ManagerGuard]},
            {path: ROUTES_PATH.reassignTask.url, component: ReasignarTareaComponent, canActivate: [ManagerGuard]},
            {path: ROUTES_PATH.gestionPagosPsd.url, component: GestionPSDComponent, canActivate: [ManagerGuard]},
            {path: ROUTES_PATH.abonoDevolucion.url, component: AbonoDevolucionComponent, canActivate: [ManagerGuard]},
            {
                path: ROUTES_PATH.enrutadorPantalla.url + '/:redirect',
                component: EnrutadorPantallasComponent,
                canActivate: [ManagerGuard]
            },
        ]
    }
];

export const AppRoutes = RouterModule.forRoot(
    routes,
    {useHash: true}
)
