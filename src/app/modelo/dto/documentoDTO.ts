export class DocumentoDTO {
    seleccion: boolean;
    nombreDocumento: string;
    tipoDocumental: string;
    documento: string;
    fechaCreacion?: Date;
    tamano?: string;
    tipoArchivo?: string;
}