export class RequestCreateTransactionDTO {
    entityCode: string;
    srvCode: string;
    transValue: string;
    urlRedirect: string;
    referenceArray: string[];
}
