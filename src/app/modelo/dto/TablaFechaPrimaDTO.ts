export interface TablaFechaPrimaDTO {

    fechaInicio: string;
    fechaFinal: string;
    tipoPrima: string;
    valorSobrePrima: number;
    porcentaje: number;
}
