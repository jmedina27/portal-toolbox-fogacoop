export class DatosContactoDTO {
    email: string;
    direccion: string;
    telefono: string;
    celular: string;
}