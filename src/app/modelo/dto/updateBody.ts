import {RolesDTO} from "./rolesDTO";

export class UpdateBody {

    id?: string;
    username?: string;
    email?: string;
    name?: string;
    phoneNumber?: string;
    ldapGuid?: string;
    organization?: string;
    roles?: string[];
   }
