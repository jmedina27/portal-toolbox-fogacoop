export class bodyChangedPasswordDTO {
    userName?: string;
    oldPassword?: string;
    newPassword?: string;
}
