import {RolesDTO} from "../../../../rolesDTO";

export class BodyDTO {

     id?: string;
     name?: string;
     username?: string;
     password?: string;
     email?: string;
     phoneNumber?: string;
     userName?: string;
     roles?: RolesDTO[];
     enabled?: boolean;
     ldapGuid?: string;
     token?: string;
     organization?: string;
     lockoutEndDateUt?: Date;
     lockoutEnabled?: boolean;

}
