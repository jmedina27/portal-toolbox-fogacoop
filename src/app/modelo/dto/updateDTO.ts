import {headerDTO} from "./common/domain/generic/user/headerDTO";
import {UpdateBody} from "./updateBody";


export class UpdateDTO {

    header: headerDTO ;
    body: UpdateBody;
}
