import { DatosContactoDTO } from "./datosContactoDTO";

export class ParticipanteDTO {
    numeroDocIdent: string;
    tipoDocIdent: string;
    primerApellido: string;
    primerNombre: string;
    segundoApellido: string;
    segundoNombre: string;
    edad: number;
    relacion: string;
    datosContacto: DatosContactoDTO;
}

