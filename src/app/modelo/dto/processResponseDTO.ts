import { ContenedorDTO } from "./contenedorDTO";
import { ResponseDTO } from "./responseDTO";

export class ProcessResponseDTO {
    containers: Array<ContenedorDTO>
    response: ResponseDTO;
}