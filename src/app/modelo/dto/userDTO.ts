import { headerDTO } from "./common/domain/generic/user/headerDTO";
import { BodyDTO } from "./common/domain/generic/user/bodyDTO";

export class UserDTO {

     header: headerDTO ;
     body: BodyDTO ;


}
