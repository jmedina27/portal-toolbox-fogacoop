export class NameValuePair {
    name: string;
    value: string;
}
