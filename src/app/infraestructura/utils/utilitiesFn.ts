import { Injectable } from "@angular/core";
import { isNullOrUndefined } from "util";

@Injectable()
export class UtilitiesFnService {
    
    formToDto = function (form, entity) {
        if (!!form && !!entity) {
            if (!!Object.keys(form) && !!Object.keys(entity)){
                for (let index = 0; index < Object.keys(form).length; index++) {
                    var key = Object.keys(form)[index];
                    if(!isNullOrUndefined(key)){ 
                        var element = Object(form)[key];
                        if (!!element)
                            Object(entity)[key] = element;
                    }                        
                }
            }
        }
        return entity
    }

    dtoToForm = function (form, entity) {
        var keys = Object.keys(entity);
        if (keys.length > 0) {
            for (let index = 0; index < keys.length; index++) {
                if (this.validateKey(Object.keys(form), keys[index]))
                    form[keys[index]].setValue(entity[keys[index]]);
            }
        }
        return form;
    }

    validateKey = function (entity, key) {
        let band = false;
        for (let index = 0; index < entity.length; index++) {
            if (entity[index] == key) {
                band = true;
                break;
            }
        }
        return band;
    }

    loadNvpToValue = function (list, item) {
        return list.find(function(element) {
            return element.value == item;
        });
    }

    calculateAge = function (birthday) { 
        let age = (new Date().getTime() - new Date(birthday).getTime()) / 31557600000;
            age = Math.floor( age );
        return age;
    }

    compareDataToEntity = function (data: any[], Entity: any[]) {
        for (let idx = 0; idx < data.length; idx++) {
          const element = !!data[idx].tipo ? data[idx].tipo.codigo : "";
          for (let idy = 0; idy < Entity.length; idy++) {
            if (Entity[idy].code === element) {
              Entity[idy].value = data[idx].aplica.toString();
              break;
            }
          }
        }
        return Entity;
    }

    deleteItemByEntity (entity: any, item: any) : any[] {
        entity.splice(entity.indexOf(item),1);
        return entity;
    }

    updateItemByEntity (entity, item: any, index: number) : any [] {
        if (index >= 0)
            return entity[index] = item
        else
            return entity.push(item)
    }

    filterItemByListNvp (list: any[], element: any) : any {
        var aux = list.filter( item => {
            return item.value == element;
        });
        return !!aux ? aux[0] : {};
    }

    filterItemByName (list: any[], element: any) : any {
        var aux = list.filter( item => {
            return item.name == element;
        });
        return !!aux ? aux[0] : {};
    }

    validateString (value) {
        return !!value ? String(value) : "";
    }

    getRandomInt (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}