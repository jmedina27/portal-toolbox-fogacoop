import { ApiManagerService } from "../api/api-manager";
import { HttpHandlerService } from "./httpHandler";
import { UtilitiesFnService } from "./utilitiesFn";

export const RESOURCES = [
    HttpHandlerService,
    ApiManagerService,
    UtilitiesFnService
]