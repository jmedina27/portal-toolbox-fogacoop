import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertService } from 'src/app/infraestructura/utils/alert.service';
import { Message } from 'primeng/components/common/api';
import { failureType } from 'src/environments/environment.variables';

@Component({
    selector: 'alert',
    templateUrl: 'alert.component.html'
})

export class AlertComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    msgs: Message[] = [];

    constructor(private alertService: AlertService) { }

    ngOnInit() {
        this.subscription = this.alertService.getMessage().subscribe(message => { 
            if (!!message) {
                //debugger
                var msg = this.chargeMessage(message);
                this.msgs.push({severity:message.type, summary:msg, detail: ' Validation failed '});
            }
        });
    }

    chargeMessage (message: any) : string {
        var errorType = "";
        if (!!message.text.type) {
            errorType = String(message.text.type).toUpperCase();
        } else if (!!message.type) {
            errorType = String(message.type).toUpperCase();
        }
        switch (errorType) {
            case failureType.error:
                return message.text.message;
            case failureType.fail:
                return message.text.msg;
            default:
                break;
        }
        return message.text;
    }
    

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
