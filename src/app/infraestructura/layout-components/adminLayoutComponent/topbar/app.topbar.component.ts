import { Component } from '@angular/core';
import { AdminLayoutComponent } from '../adminLayout/adminLayout.component';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopbarComponent {

    constructor (
        public app: AdminLayoutComponent
    ) {}

}
