import { ROUTES_PATH } from "src/app/app.routes.names";
import {ADMINISTRADOR, COOPERATIVA, USUARIO} from "../../../../app.roles";

export  const MENU_OPTIONS = [

];

export let FOR_ROLE_OPTIONS = {};

FOR_ROLE_OPTIONS[ADMINISTRADOR] = [
    {label: 'Tablero de Proceso', icon: 'dashboard', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.dashboard.url]},
    {label: 'Procesos', icon: 'work', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.processes.url]},
    {label: 'Tareas', icon: 'list', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.task.url]},
    {label: 'Reasignar Tareas', icon: 'subject', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.reassignTask.url]},
    {label: 'Registrar Pago', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' +ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.registrarPagoConsignacion.url]},
    {label: 'Información Pago PSE', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.informacionPagoPSE.url]},
    {label: 'Confirmar pago', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/'+ ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.confirmarPago.url]},
    {label: 'Movimientos Cooperativa', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' +ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.consultaCooperativa.url]},
    {label: 'Movimientos Fogacoop', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' +ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.consultaFogacoop.url]},
    {label: 'Pago no Liquidado', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.enrutadorPantalla.url + '/' +ROUTES_PATH.informacionPagoNoLiquidado.url]},
    {label: 'Gestion Pagos PSD', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.gestionPagosPsd.url]},
    {label: 'Confirmar Abono', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.abonoDevolucion.url]}
];

FOR_ROLE_OPTIONS[COOPERATIVA]= [
    {label: 'Información Pago PSE', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.informacionPagoPSE.url]},
    {label: 'Confirmar pago', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/'+ ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.confirmarPago.url]},
    {label: 'Movimientos Cooperativa', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' +ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.consultaCooperativa.url]},
    {label: 'Movimientos Fogacoop', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' +ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.consultaFogacoop.url]},
    {label: 'Pago no Liquidado', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.enrutadorPantalla.url + '/' +ROUTES_PATH.informacionPagoNoLiquidado.url]},
    {label: 'Gestion Pagos PSD', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.gestionPagosPsd.url]},
    {label: 'Confirmar Abono', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.abonoDevolucion.url]}

];

FOR_ROLE_OPTIONS[USUARIO] =[
    {label: 'Tablero de Proceso', icon: 'dashboard', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.dashboard.url]},
    {label: 'Procesos', icon: 'work', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.processes.url]},
    {label: 'Tareas', icon: 'list', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.task.url]},
    {label: 'Reasignar Tareas', icon: 'subject', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.reassignTask.url]},
    {label: 'Registrar Pago', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.registrarPagoConsignacion.url]},
    {label: 'Información Pago PSE', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.informacionPagoPSE.url]},
    {label: 'Confirmar pago', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/'+ ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.confirmarPago.url]},
    {label: 'Movimientos Cooperativa', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' +ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.consultaCooperativa.url]},
    {label: 'Movimientos Fogacoop', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' +ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.consultaFogacoop.url]},
    {label: 'Pago no Liquidado', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.enrutadorPantalla.url + '/' +ROUTES_PATH.informacionPagoNoLiquidado.url]},
    {label: 'Gestion Pagos PSD', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.gestionPagosPsd.url]},
    {label: 'Confirmar Abono', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.abonoDevolucion.url]}

    ];
