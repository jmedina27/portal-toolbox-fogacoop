import { Component, Input, OnInit } from "@angular/core";
import { AdminLayoutComponent } from "../adminLayout/adminLayout.component";
import { ROUTES_PATH } from "src/app/app.routes.names";
import {  } from "src/app/app.routes.names";
import {FOR_ROLE_OPTIONS} from "./menu.options";
import {isNullOrUndefined} from "util";


@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    @Input() reset: boolean;

    model: any[];

    constructor(public app: AdminLayoutComponent) {}

    ngOnInit() {

        const userJson = localStorage.getItem("currentUser");
        this.model = [
            //{label: 'Tablero de Proceso', icon: 'dashboard', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.dashboard]}
        ];

        if(!userJson)
            return;

        const usuario = JSON.parse(userJson);

        (<any[]> usuario.body.roles).forEach(r => {

            if(!isNullOrUndefined(FOR_ROLE_OPTIONS[r.description])){

                FOR_ROLE_OPTIONS[r.description].forEach( route =>{
                    if(this.model.every(item => item.label != route.label))
                        this.model = [...this.model, route];
                });
            }
        });
    }
}
